<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    //
    use SoftDeletes;
    protected $fillable = [
        'name',
        'title',
        'body',
        'image',
        'password'
    ];
    protected $table = "messages";

    public static $thumbnailPath = 'storage/message/thumbnail/';
    public static $originalPath = 'storage/message/original/';


    public function imageURL($image, $type = 'thumbnail')
    {
        if (empty($image)) {
            return 'http://via.placeholder.com/300x300';
        } else {
            return '/storage/message/' . $type . '/' . $image;
        }
    }

    public function formatDate($format)
    {
        $carbon = Carbon::parse($this->created_at);
        if ($format == 'date'){
            return $carbon->format('d-m-Y');
        }
        elseif ($format == 'time'){
            return $carbon->format('H:i');
        }
        else return $carbon->format('d-m-Y H:i');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function deleteImage()
    {
        if ($this->image) {
            // unlink image in original and also thumbnail path
            if (file_exists($thumbnailImage = public_path() . Message::$thumbnailPath . $this->image)) {
                unlink($thumbnailImage);
            }
            if (file_exists($originalImage = public_path() . Message::$originalPath . $this->image)) {
                unlink($originalImage);
            }
        }
    }
}
