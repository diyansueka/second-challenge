<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Message;
use Illuminate\Contracts\Validation\Validator;


class UpdateMessage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'editName'      => 'nullable|min:3|max:16',
            'editTitle'   => 'required|min:10|max:32',
            'editBody'      => 'required|min:10|max:200',
            'editImage'     => 'nullable|image|mimes:jpeg,jpg,png,gif|max:1024'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $message = Message::findOrFail($this->get('id'));

        $message->input_password = $this->password;

        return redirect()
            ->back()
            ->with([
                'modal'         => 'editModal',
                'data'          => $message,
            ])
            ->withErrors($validator, 'modal');
    }
}
