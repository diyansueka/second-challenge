<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMessage extends FormRequest
{
    protected $errorBag = 'index';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    // public function authorize()
    // {
    //     return true;
    // }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'nullable|min:3|max:16',
            'title'    => 'required|min:10|max:32',
            'body'     => 'required|min:10|max:200',
            'password' => 'numeric|nullable|digits:4',
            'image'    => 'nullable|mimes:jpeg,jpg,png,gif|max:1024'
        ];
    }
}
