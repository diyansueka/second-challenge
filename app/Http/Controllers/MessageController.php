<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\StoreMessage;
use App\Http\Requests\UpdateMessage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Image;
use File;

class MessageController extends Controller
{
    //
     /**
     * Display a listing of the resource.
     *
     */

    public function index()
    {
        // paginate the view, 10 message per page
        $messages = Message::orderBy('created_at', 'desc')->paginate(5);
        return view('user.home.index', compact('messages'));
    }

    /**
     * Store new message to db.
     *
     */

    public function store(StoreMessage $request)
    {
        $input = $request->all();

        $user = Auth::user();

        if ($originalImage = $request->file('image')){
            // upload image and move image to correct folder
            $input['image'] = $this->saveImage($originalImage);
        }

        // create new message model
        if($user){
            $user->messages()->create($input);
        } else {
            Message::create($input);
        }

        return redirect('/')->with($this->successMessage('added'));
    }

    /**
     * Pass edit value to edit modal and redirect to previouse page
     *
     */
    public function edit(Request $request, Message $message)
    {
        // get message data to be shown in edit modal
        //dd($request->inputPassword);
        //$message = Message::findOrFail($request->id);

        // check and compare message password
        $result = $this->checkPassword($message->password, $request->inputPassword, "edit");

        if (Auth::guest()) {
            $message->input_password = $request->inputPassword;
        }

        return redirect()
            ->back()
            ->with(
                $this->redirectModal('editModal', $message, $result)
            );
    }

    /**
     * Update edited message
     *
     */
    public function update(UpdateMessage $request, Message $message)
    {
        $input = $request->except('password');
        //$message = Message::findOrFail($request->id);

        $result = $this->checkPassword($message->password, $request->password, "edit");

        $this->checkUserAuth($result, $message);

        // if ($result['status'] === 2){
        //     return redirect()->back()->with('error_post', "You Can't Update the Message");
        // }

        $imageName = $message->image;

        //if ($request->validated()){
            // if delete image is checked
            if (($request->deleteImage)) {
                // delete function for image
                $message->deleteImage();
                // $this->deleteImage($message);
                // set image to null
                $message->image = NULL;

            } else {
                if (($request->editedImage)){
                    // if image selected and delete image is not checked
                    // upload image and move the image in the folder
                    if ($originalImage = $request->file('editedImage')) {
                        // get image name
                        $message->image = $this->saveImage($originalImage);
                    }
                }
            }

            // $message->title = $request->editedTitle;
            // $message->body = $request->editedBody;
            //$message->save();

        //}
        $message->update($input);
        $request->session()->flush();
        return redirect()->back()->with($this->successMessage('updated'));
        //return redirect()->route('index')->with($this->successMessage('updated'));
    }

    /**
     * Pass message value to delete modal
     *
     */
    public function delete(Request $request, Message $message)
    {
        //$message = Message::find($request->id);

        // check current password status return integer value
        $result = $this->checkPassword($message->password, $request->inputPassword,'delete');
        if (Auth::guest()) {
            $message->input_password = $request->inputPassword;
        }
        // pass data to delete modal
        return redirect()
            ->back()
            ->with(
                $this->redirectModal('deleteModal', $message, $result)
            );
    }

    /**
     * Delete message
     *
     */
    public function destroy(Request $request, Message $message)
    {
        //$message = Message::find($request->id);

        $result = $this->checkPassword($message->password, $request->password, "edit");

        $this->checkUserAuth($result, $message);

        // delete image message
        $message->deleteImage();
        // $this->deleteImage($message);

        Message::destroy($request->id);

        // redirect to the specific page
        $result = Message::paginate(10);
        $lastPage = $result->lastPage();

        // if the message is the last
        if ($request->currentPage > $lastPage) {
            // back to the previous page
            $url = '/' . '?page=' . $lastPage;
            return redirect($url)->with($this->successMessage('deleted'));
        } else {
            return redirect()->back()->with($this->successMessage('deleted'));
        }
    }

    /**
     * Check inputed password
     *
     */
    public function checkPassword($dataPassword, $inputPassword, $modalName='')
    {
        if ($dataPassword) {
            if ($dataPassword <> $inputPassword) {
                // the password doesn't match
                return [
                    'messageNotMatch' => "The passwords you entered do not match. Please try again."
                ];
            }
        } else {
            if (Auth::guest()) {
                // the post doesn't have password
                return [
                    'messageNoPassword' => "This message can’t " . $modalName . ", because this message has not been set password."
                ];
            }
        }
    }

    /**
     * Save image to folder and db
     *
     */
    public function saveImage($originalImage)
    {
        // get and set image name
        $name = time() . $originalImage->getClientOriginalName();
        // get original image
        $dataImage = Image::make($originalImage);
        // check for path avaibility
        if ( ! \File::isDirectory($originalPath = public_path() . '/' . Message::$originalPath) ) {
            // create new one if not exist
            \File::makeDirectory($originalPath, 493, true);
        }

        if ( ! \File::isDirectory($thumbnailPath = public_path() . '/' .Message::$thumbnailPath) ) {
            \File::makeDirectory($thumbnailPath, 493, true);
        }
        // save original image
        $dataImage->save($originalPath . $name);
        // resize image for thumbnail
        $thumbnailImage = $dataImage->resize(150, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        // save thumbnail image
        $thumbnailImage->save($thumbnailPath . $name);

        return $name;
    }

    /**
     * Delete image from folder and db
     *
     */
    // public function deleteImage($message)
    // {
    //     if ($message->image) {
    //         // unlink image in original and also thumbnail path
    //         if (file_exists($thumbnailImage = Message::$thumbnailPath . $message->image)) {
    //             unlink($thumbnailImage);
    //         }
    //         if (file_exists($originalImage = Message::$originalPath . $message->image)) {
    //             unlink($originalImage);
    //         }
    //     }
    // }

    public function redirectModal($modal, $message, $result)
    {
        // new refactor redirect
        return [
            'modal'   => $modal,
            'data'    => $message,
            'result'   => $result
        ];
    }

    protected function successMessage($action)
    {
        return [
            "success" => "Message  " . $action . "  successfully"
        ];
    }

    public function checkUserAuth($result, $message)
    {
        if ( Auth::guest() && !is_null($result) || Auth::user() && !$message->user_id == Auth::id() ) {
            return abort(403, 'Unauthorized action.');
        }
    }
}
