<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $posts = Post::withTrashed()->orderBy('id', 'desc')->paginate(10);
        // return view('admin.post.index', compact('posts'));
        $messages = Message::withTrashed()->orderBy('id', 'desc')->paginate(10);
        return view('admin.message.index', compact('messages'));

    }

    public function remove(Request $request, Message $message)
    {
        $message->delete();
        return redirect()->back();
    }

    public function recover(Request $request)
    {
        $id = $request->id;

        $message = Message::onlyTrashed()->where('id',$id);

        $message->restore();

        return redirect()->back();
    }

    public function removeImage(Request $request, Message $message)
    {
        $input['image'] = NULL;

        $message->update($input);

        $message->deleteImage();

        return redirect()->back();
    }

    public function removeChecked(Request $request)
    {
        $messageIDArray = explode(",", $request->id_delete);

        Message::destroy($messageIDArray);

        return redirect()->back();
    }

    public function search(Request $request)
    {
        $params = $request->except('_token');

        $query = Message::withTrashed()->where([
            ['title', 'LIKE', "%{$request->title}%"],
            ['body', 'LIKE', "%{$request->body}%"]
        ]);

        if ($request->imageOption === 'with') {
            $query->where('image', '<>', NULL);
        } else if ($request->imageOption === 'without') {
            $query->where('image', '=', NULL);
        }

        if ($request->statusOption === 'on') {
            $query->where('deleted_at', '=', NULL);
        } else if ($request->statusOption === 'delete') {
            $query->where('deleted_at', '<>', NULL);
        }

        $messages = $query->orderBy('id', 'desc')->paginate(10);

        $messages->appends($params);
        // dd($messages);
        return view('admin.message.index', compact('messages', 'params'));
    }
}

