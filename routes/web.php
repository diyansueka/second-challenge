<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::get('/', 'MessageController@index')->name('index');
Route::post('/store', 'MessageController@store')->name('store');

Route::post('/delete/{message}', 'MessageController@delete')->name('delete');
Route::post('/edit/{message}', 'MessageController@edit')->name('edit');

Route::post('/destroy/{message}', 'MessageController@destroy')->name('destroy');
Route::post('/update/{message}', 'MessageController@update')->name('update');

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/member-register', 'Auth\RegisterController@membershipRegisterForm')->name('member-register');

Route::get('/user-verified', function() {
    return view('auth.user-verified');
});

Route::post('/register-back', 'Auth\RegisterController@registerBack')->name('register-back');

Route::group(['prefix' => 'admin','middleware' => 'admin'], function () {
    Route::get('/', 'AdminController@index');
    Route::post('/message/remove/{message}', 'AdminController@remove');
    Route::post('/message/remove-checked/', 'AdminController@removeChecked');
    Route::post('/message/remove-image/{message}', 'AdminController@removeImage');
    Route::post('/message/recover', 'AdminController@recover');
    Route::get('/message/search', 'AdminController@search');
});
