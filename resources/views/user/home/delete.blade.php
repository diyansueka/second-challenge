<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Delete Message</h4>
                @if(!is_null(session('result')))
                    <p class="message-error">{{ session('result.messageNoPassword') }} {{ session('result.messageNotMatch') }}</p>
                @endif
            </div>
            <form >
                <div class="modal-body">
                    <div class="form-group">
                        <input name="id" type="hidden" value="{{ $data->id }}">
                    </div> 
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" id="deleteTitle" name="editedTitle" readonly value="{{ $data->title }}" >
                    </div>
                    <div class="form-group">
                        <label>Body</label>
                        <textarea rows="5" class="form-control" id="deleteBody" name="editedBody" readonly>{{ $data->body }}</textarea>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <img class="img-responsive" alt="" src="{{ $data->imageURL($data->image, 'thumbnail') }}"> 
                        </div>
                    </div>
                </div>
            </form>

            <div class="modal-footer">
                @if(!is_null(session('result.messageNotMatch')))
                    <form method="POST" class="form-inline">
                        @csrf
                        <div class="form-group">
                            <label for="inputPassword" class="sr-only">Password</label>
                            <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Password">
                            <input name="id" type="hidden" value="{{ session('data.id') }}">
                        </div>
                        <button type="submit" formaction="{{ route('delete', $data->id) }}" class="btn btn-default mb-2" data-toggle="modal">
                            Re-Enter Password
                        </button>
                    </form>
                @else
                    <form method="POST" class="form-inline">
                        @csrf
                        <input name="id" type="hidden" value="{{ $data->id }}">
                        <input name="password" type="hidden" value="{{ session('data.password') }}">
                        <input name="currentPage" type="hidden" value="{{ $messages->currentPage() }}">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" formaction="{{ route('destroy', $data->id) }}" class="btn btn-danger">Delete</button>
                    </form>
                @endif

            </div>
        </div>
    </div>
</div>