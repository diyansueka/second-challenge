<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Edit Message</h4>
                @if(!is_null(session('result')))
                    <p class="message-error">{{ session('result.messageNoPassword') }} {{ session('result.messageNotMatch') }}</p>
                @endif
                <script>readOnly(false);</script>
            </div>
            <form method="POST" enctype="multipart/form-data" id="editFrm">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <input name="id" type="hidden" value="{{ $data->id }}">
                    </div>
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" id="editName" name="name" 
                            value="{{ old('name', $data->name) }}">
                        <p class="small text-danger mt-5">{{ $errors->modal->first('name') }}</p>
                    </div> 
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" id="editTitle" name="editedTitle" value="{{ $data->title }}" >
                            <p class="small text-danger mt-5">{{ $errors->modal->first('editedTitle') }}</p>
                    </div>
                    <div class="form-group">
                        <label>Body</label>
                        <textarea rows="5" class="form-control" id="editBody" name="editedBody">{{ $data->body }}</textarea>
                            <p class="small text-danger mt-5">{{ $errors->modal->first('body') }}</p>
                    </div>
                    @if(is_null(session('result.messageNotMatch')))
                        <div class="form-group row">
                            <div class="col-md-4">
                                <img class="img-responsive" alt="" src="{{ $data->imageURL($data->image, 'thumbnail') }}"> 
                            </div>
                            <div class="col-md-8 pl-0">
                                <label>Choose image from your computer :</label>
                                <div class="input-group">
                                    <input type="text" class="form-control upload-form" value="No file chosen" readonly>
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                            <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="editedImage" id="editBtn" multiple>
                                        </span>
                                    </span>
                                </div>
                                    <p class="small text-danger mt-5">{{ $errors->modal->first('editedImage') }}</p>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="editCheckBox" name="deleteImage">Delete image
                                    </label>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    @if(!is_null(session('result.messageNotMatch')))
                        <form method="POST" class="form-inline">
                            @csrf
                            <div class="form-group">
                                <label for="inputPassword" class="sr-only">Password</label>
                                <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Password">
                            </div>
                            <button type="submit" formaction="{{ route('edit', $data->id) }}" class="btn btn-default mb-2" data-toggle="modal">
                                Re-Enter Password
                            </button>
                        </form>
                        <script>readOnly(true);</script>
                    @else
                        <form method="POST" enctype="multipart/form-data">
                            @csrf
                            <input name="password" type="hidden" value="{{ session('data.password') }}">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" formaction="{{ route('update', $data->id) }}" class="btn btn-primary">Save changes</button>
                        </form>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>