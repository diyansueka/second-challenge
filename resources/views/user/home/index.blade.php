@extends('user.layout.main')

@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if (session('error_post'))
        <div class="alert alert-danger">
            {{ session('error_post') }}
        </div>
    @endif

    @php
        $user = Auth::user();
    @endphp

    <form method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label>Name</label>
            @auth
                <input type="text" class="form-control" 
                    id="name" name="name" value="{{ $errors->hasBag('index') ? old('name') : $user->name }}">
            @else
                <input type="text" class="form-control" 
                    id="name" name="name" value="{{ $errors->hasBag('index') ? old('name') : '' }}">
            @endauth
            <p class="small text-danger mt-5">{{ $errors->index->first('name') }}</p>
        </div>
        <div class="form-group">
            <label>Title</label>
            <input type="text" class="form-control @error('title') is-invalid @enderror" 
                id="title" name="title" value="{{ old('title') }}">
                <p class="small text-danger mt-5">{{ $errors->index->first('title') }}</p>
        </div>
        <div class="form-group">
            <label>Body</label>
            <textarea rows="5" class="form-control @error('body') is-invalid @enderror" 
                id="body" name="body">{{ old('body') }}</textarea>
                <p class="small text-danger mt-5">{{ $errors->index->first('body') }}</p>
        </div>
        <div class="form-group">
            <label>Choose image from your computer :</label>
            <div class="input-group">
                <input type="text" class="form-control upload-form" value="No file chosen" readonly>
                <span class="input-group-btn">
                    <span class="btn btn-default btn-file">
                        <i class="fa fa-folder-open"></i>&nbsp;Browse <input id="image" name="image" type="file" multiple>
                    </span>
                </span>
            </div>
                <p class="small text-danger mt-5">{{ $errors->index->first('image') }}</p>
        </div>
        @guest
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control @error('password') is-invalid @enderror" 
                    id="password" name="password" value="{{ old('password') }}">
                @error('password')
                    <p class="small text-danger mt-5">{{ $message }}</p>
                @enderror
            </div>
        @endguest
        <div class="text-center mt-30 mb-30">
            <button type="submit" formaction="{{ route('store') }}" class="btn btn-primary">Submit</button>
        </div>
    </form>

    @foreach ($messages as $message)
            <div class="post">
                <div class="clearfix">
                    <div class="pull-left">
                        <h2 class="mb-5 text-green"><b>{{ $message->title }}</b></h2>
                    </div>
                    <div class="pull-right text-right">
                        <p class="text-lgray">
                            {{ $message->formatDate('date') }}
                            <br/>
                            <span class="small">{{ $message->formatDate('time')}}</span>
                        </p>
                    </div>
                </div>
                <h4 class="mb-20">
                        <p>Name : {{ $message->name }}</p>
                        @if ($message->user)
                            <span class="text-id">[ ID : {{ $message->user->id }} ]</span>
                        @endif
                    </h4>
                <p>{{ $message->body }}</p>
                <div class="img-box my-10">
                    <img class="img-responsive img-post"
                    src="{{ $message->imageURL($message->image, 'thumbnail') }}"
                    alt="image">
                </div>
                
                    <form method="POST" class="form-inline mt-50">
                        @csrf
                        @auth
                            @if ($message->user_id === $user->id)
                                <button type="submit" formaction="{{ route('edit', $message->id) }}" class="btn btn-default mb-2" data-toggle="modal">
                                    <i class="fa fa-pencil p-3"></i>
                                </button>
                                <button type="submit" formaction="{{ route('delete', $message->id) }}" class="btn btn-danger mb-2" data-toggle="modal">
                                    <i class="fa fa-trash p-3"></i>
                                </button>
                            @endif
                        @else
                            @if (!$message->user_id)
                                <div class="form-group mx-sm-3 mb-2">
                                    <label for="inputPassword" class="sr-only">Password</label>
                                    <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Password">
                                    <input name="id" id="messageId" type="hidden" value="{{ $message->id }}">
                                </div>
                                <button type="submit" formaction="{{ route('edit', $message->id) }}" class="btn btn-default mb-2" data-toggle="modal">
                                    <i class="fa fa-pencil p-3"></i>
                                </button>
                                <button type="submit" formaction="{{ route('delete', $message->id) }}" class="btn btn-danger mb-2" data-toggle="modal">
                                    <i class="fa fa-trash p-3"></i>
                                </button>
                            @endif
                        @endauth

                    </form>  
            </div>   
    @endforeach

    <div class="text-center mt-30">
        {{ $messages->links() }}
    </div>

@endsection