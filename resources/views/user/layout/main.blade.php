<html>
    <head>
        <title>Timedoor Challenge</title>
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/font-awesome.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/tmdrPreset.css') }}">
        <!-- CSS End -->

        <!-- Javascript -->
        <script type="text/javascript" src="{{ URL::asset('user/js/jquery.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('user/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('user/js/main.js') }}"></script>
        <!-- <script>hideId()</script> -->
         @if(Session::has('modal'))
            @if (session('modal') === "editModal")
                <script>modal('editModal');</script>
            @else
                <script>modal('deleteModal');</script>
            @endif
        @endif
        @php
            $data = Session::get('data');
        @endphp
    </head>

    <body class="bg-lgray">
        <header>
        @include('user.layout.header')
        </header>
        <main>
            <div class="section">
                <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 bg-white p-30 box">
                        <div class="text-center">
                            <h1 class="text-green mb-30"><b>Second Challenge</b></h1>
                        </div>
                            @yield('content')
                    </div>
                </div>
                </div>
            </div>
        </main>
        
        <footer>
            @include('user.layout.footer')
        </footer>
        {{-- Modal --}}
        @if ($data)
            @include('user.home.edit')
            @include('user.home.delete')
        @endif
    </body>
</html>