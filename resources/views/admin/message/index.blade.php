@extends('admin.layout.main')
@section('content')
<div class="row">
    <div class="col-xs-12"><!-- /.col-xs-12 -->
        <div class="box box-success">
            <div class="box-header with-border">
                <h1 class="font-18 m-0">Timedoor Challenge - Level 9</h1>
            </div>
            {{-- <form method="" action=""> --}}
                <div class="box-body">
                    <div class="bordered-box mb-20">
                        <form class="form" role="form" name="form_search" id="form_search" action="/admin/message/search" method="get">
                            <table class="table table-no-border mb-0">
                                <tbody>
                                    <tr>
                                        <td width="80"><b>Title</b></td>
                                        <td>
                                            <div class="form-group mb-0">
                                                <input type="text" class="form-control" name="title" value="{{ $params['title'] ?? "" }}" id="title">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Body</b></td>
                                        <td>
                                            <div class="form-group mb-0">
                                                <input type="text" class="form-control" name="body" value="{{ $params['body'] ?? "" }}" id="body">
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-search">
                                <tbody>
                                    <tr>
                                        <td width="80"><b>Image</b></td>
                                        <td width="60">
                                            <label class="radio-inline">
                                                <input type="radio" name="imageOption" value="with" {{ empty($params['imageOption']) ? "" : $params['imageOption'] === "with" ? "checked" : "" }}> with
                                            </label>
                                        </td>
                                        <td width="80">
                                            <label class="radio-inline">
                                                <input type="radio" name="imageOption" value="without" {{ empty($params['imageOption']) ? "" : $params['imageOption'] === "without" ? "checked" : "" }}> without
                                            </label>
                                        </td>
                                        <td>
                                            <label class="radio-inline">
                                                <input type="radio" name="imageOption" value="unspecified" {{ empty($params['imageOption']) ? "checked" : $params['imageOption'] === "unspecified" ? "checked" : "" }}> unspecified
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="80"><b>Status</b></td>
                                        <td>
                                            <label class="radio-inline">
                                                <input type="radio" name="statusOption" value="on" {{ empty($params['statusOption']) ? "" : $params['statusOption'] === "on" ? "checked" : "" }}> on
                                            </label>
                                        </td>
                                        <td>
                                            <label class="radio-inline">
                                                <input type="radio" name="statusOption" value="delete" {{ empty($params['statusOption']) ? "" : $params['statusOption'] === "delete" ? "checked" : "" }}> delete
                                            </label>
                                        </td>
                                        <td>
                                            <label class="radio-inline">
                                                <input type="radio" name="statusOption" value="unspecified" {{ empty($params['statusOption']) ? "checked" : $params['statusOption'] === "unspecified" ? "checked" : "" }}> unspecified
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button type="submit" class="btn btn-default mt-10">
                                                <i class="fa fa-search"></i> Search
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th><input id="checkbox-header" type="checkbox"></th>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Body</th>
                                <th width="200">Image</th>
                                <th>Date</th>
                                <th width="50">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($messages as $message)
                                @if (!$message->deleted_at)
                                    <tr>
                                        <td><input value="{{ $message->id }}" class="checkbox-message" type="checkbox"></td>
                                        <td>{{ $message->id }}</td>
                                        <td>{{ $message->title }}</td>
                                        <td>{{ $message->body }}</td>
                                        <td>
                                            @if ($message->image)
                                                <img class="img-prev" src="{{ $message->imageURL($message->image) }}">
                                                <a href="#" data-toggle="modal" data-target="#deleteModal" class="btn btn-danger" rel="tooltip" title="Delete" onclick="_delete('delete_image', {{ $message->id }});return false;">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            @endif
                                        </td>
                                        <td>{{ $message->created_at->format('Y/m/d') }}<br><span class="small">{{ $message->created_at->format('H:i:s') }}</span></td>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#deleteModal" class="btn btn-danger" rel="tooltip" title="Delete" onclick="_delete('delete_message', {{ $message->id }});return false;">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @else
                                    <tr class="bg-gray-light">
                                        <td>&nbsp;</td>
                                        <td>{{ $message->id }}</td>
                                        <td>{{ $message->title }}</td>
                                        <td>{{ $message->body }}</td>
                                        <td>
                                            @if ($message->image)
                                                <img class="img-prev" src="{{ $message->imageURL($message->image) }}">
                                            @endif
                                        </td>
                                        <td>{{ $message->created_at->format('Y/m/d') }}<br><span class="small">{{ $message->created_at->format('H:i:s') }}</span></td>
                                        <td>
                                            <form name="form_recover" id="form_recover" action="/admin/message/recover" method="post">
                                                @csrf
                                                <input name="currentPage" type="hidden" value="{{ $messages->currentPage() }}">
                                                <input name="id" type="hidden" value="{{ $message->id }}">
                                                <button type="submit" class="btn btn-default" title="Recover">
                                                    <i class="fa fa-repeat"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                    <a class="btn btn-default mt-5" onclick="delete_checked();">Delete Checked Items</a>
                    <div class="text-center">
                        {{-- Pagination --}}
                        {{ $messages->onEachSide(5)->links() }}
                    </div>
                </div>
            {{-- </form> --}}
        </div>
    </div>
</div>
@endsection

@section('javascript')
    <script>
        function _delete(act, id) {
            if (act === "delete_message") {
                $("#form_delete").attr("action", "/admin/message/remove/" + id);
            }
            else if (act === "delete_image") {
                $("#form_delete").attr("action", "/admin/message/remove-image/" + id);
            }
        }
    </script>

    <script>
        $('#checkbox-header').click(function ()
        {
            $('input[type=checkbox]').not(":disabled").prop('checked', this.checked);
        });

        function delete_checked()
        {
            var id = [];

            $('.checkbox-message:checked').each(function()
            {
                id.push($(this).val());
            });

            if (id.length > 0) {
                $("#form_delete").attr("action", "/admin/message/remove-checked");
                $("#id_delete").attr("value", id);
                $("#deleteModal").modal()
            } else {
                alert("Please select atleast one record to delete.");
            }
        }
    </script>
@endsection
