<section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="<?php if ($page == 'home') { echo 'active'; } ?>">
        <a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> 
          <span>Dashboard</span>
        </a>
      </li>
    </ul>
</section>