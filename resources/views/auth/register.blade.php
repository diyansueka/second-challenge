<html>
    <head>
        <title>Timedoor Challenge - Level 8 | Register</title>

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/tmdrPreset.css') }}">
        <!-- CSS End -->

        <!-- Javascript -->
        <script type="text/javascript" src="{{ URL::asset('user/js/jquery.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('user/js/bootstrap.min.js') }}"></script>
        <!-- Javascript End -->
    </head>

    <body id="login">
        <div class="box login-box">
            <div class="login-box-head">
                <h1 class="mb-5">Register</h1>
                <p class="text-lgray">Please fill the information below...</p>
            </div>
            <form method="POST" action="{{ route('member-register') }}">
                @csrf
                <div class="login-box-body">
                    <div class="form-group">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name">
                        @error('name')
                            <p class="mt-5 small text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-mail">
                        @error('email')
                            <p class="mt-5 small text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input id="password" type="password" class="form-control" name="password" placeholder="Password">
                        @error('password')
                            <p class="mt-5 small text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div class="login-box-footer">
                    <div class="text-right">
                        <a href="{{ route('login') }}" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary">Confirm</button>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>