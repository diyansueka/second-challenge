<html>
    <head>
        <title>Timedoor Challenge - Level 8 | Register Confirm</title>

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/tmdrPreset.css') }}">
        <!-- CSS End -->

        <!-- Javascript -->
        <script type="text/javascript" src="{{ URL::asset('user/js/jquery.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('user/js/bootstrap.min.js') }}"></script>
        <!-- Javascript End -->
    </head>

    <body id="login">
        <div class="box login-box">
            <div class="login-box-head">
                <h1>Register</h1>
            </div>
            <form method="POST" class="form-inline">
                @csrf
                <div class="login-box-body">
                    <table class="table table-no-border">
                        <tbody>
                            <fieldset disabled>
                                <tr>
                                    <th>Name</th>
                                    <td>{{ $request->name }}</td>
                                </tr>
                                <tr>
                                    <th>E-mail</th>
                                    <td>{{ $request->email }}</td>
                                </tr>
                                <tr>
                                    <th>Password</th>
                                    <td>{{ $request->password }}</td>
                                </tr>
                            </fieldset>
                        </tbody>
                    </table>
                </div>
                <div class="login-box-footer">
                    <input type="hidden" name="name" value="{{ $request->name }}">
                    <input type="hidden" name="email" value="{{ $request->email }}">
                    <input type="hidden" name="password" value="{{ $request->password }}">
                    <div class="text-right">
                        <button formaction="{{ route('register-back') }}" type="submit" class="btn btn-default">Back</button>
                        <button formaction="{{ route('register') }}" type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </body>

</html>