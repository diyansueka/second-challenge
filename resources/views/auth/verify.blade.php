@extends('user.layout.main')
{{-- @extends('layouts.app') --}}

@section('content')
<div class="card">
    @if (session('resent'))
        <div class="alert alert-success" role="alert">
            {{ __('A fresh verification link has been sent to your email address.') }}
        </div>
    @elseif (session('invalidEmail'))
        <div class="alert alert-danger" role="alert">
            {{ __('This email has not been registered yet.') }}
        </div>
    @endif

    <h4>
        {{ __('Verify Your Email Address') }}
    </h4>
    
    <div class="card-body">

        {{ __('Before login, please check your email for a verification link.') }}
        {{ __('If you did not receive the email') }}, You can resend the verification email.
        <form method="GET" action="{{ route('verification.resend') }}">
            @csrf
            <div class="login-box-body">
                <div class="form-group mt-20">
                    <label>Email :</label>
                    <input id="email" type="email" class="form-control" name="email" placeholder="E-mail">
                </div>
            </div>
            <div class="login-box-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Resend</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
